# Java RPG (Role playing game)

### Hero
Each Hero has attributes which determine their power. They start at level 1 and has ability to level up as they gain experience.
Mages gains their power from an attribute called Intelligence, Rangers from Dexterity, Rougues from Dexterity as well, and Warriors from Strength.
There are two types of items in the game - weapons and armor. There are restrictions on which types of armor and weapon a specific hero can equip.
Equipped armor and weapon makes the hero stronger and able to deal more damage. The hero with the highest damage per second (dps) is the strongest hero.
#### Mage
Weapon: Able to equip Staff and Wand.
<br>
Armor: Able to equip Cloth.
#### Ranger
Weapon: Able to equip Bow.
<br>
Armor: Able to equip Leather and Mail.
#### Rogue
Weapon: Able to equip Dagger and Sword.
<br>
Armor: Able to equip Leather and Mail.
#### Warrior
Weapon: Able to equip Axe, Hammer and Sword.
<br>
Armor: Able to equip Mail and Plate.
