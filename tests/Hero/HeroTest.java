package Hero;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HeroTest {

    @Test
    void Hero_StartingLvl_ShouldBeOne() {

        Mage mage = new Mage("Mage1", 1);
        int actualLvl = mage.level;

        int expectedLvl = 1;

        assertEquals(expectedLvl, actualLvl);
    }

    @Test
    void Hero_LvlUp_shouldIncreaseHeroLvlByOne() {

        Mage mage = new Mage("Mage1", 1);
        mage.lvlUp(1);

        int actualLevel = mage.level;
        int expectedLevel = 2;

        assertEquals(expectedLevel, actualLevel);

    }


    // HERO ATTRIBUTES CORRECTNESS TESTS
    @Test
    void Mage_BaseAttributes_ShouldHaveCorrectValues() {

        Mage mage = new Mage("Mage1", 1);
        mage.initializeBaseAttributes();

        int actualVitality = mage.baseAttributes.vitality;
        int actualStrength = mage.baseAttributes.strength;
        int actualDexterity = mage.baseAttributes.dexterity;
        int actualIntelligence = mage.baseAttributes.intelligence;

        int expectedVitality = 5;
        int expectedStrength = 1;
        int expectedDexterity = 1;
        int expectedIntelligence = 8;

        assertTrue(
                expectedVitality == actualVitality &&
                expectedStrength == actualStrength &&
                expectedDexterity == actualDexterity &&
                expectedIntelligence == actualIntelligence
        );
    }

    @Test
    void Ranger_BaseAttributes_ShouldHaveCorrectValues() {

        Ranger ranger = new Ranger("Ranger1", 1);
        ranger.initializeBaseAttributes();

        int actualVitality = ranger.baseAttributes.vitality;
        int actualStrength = ranger.baseAttributes.strength;
        int actualDexterity = ranger.baseAttributes.dexterity;
        int actualIntelligence = ranger.baseAttributes.intelligence;

        int expectedVitality = 8;
        int expectedStrength = 1;
        int expectedDexterity = 7;
        int expectedIntelligence = 1;

        assertTrue(
                expectedVitality == actualVitality &&
                        expectedStrength == actualStrength &&
                        expectedDexterity == actualDexterity &&
                        expectedIntelligence == actualIntelligence
        );
    }

    @Test
    void Rogue_BaseAttributes_ShouldHaveCorrectValues() {

        Rogue rogue = new Rogue("Ranger1", 1);
        rogue.initializeBaseAttributes();

        int actualVitality = rogue.baseAttributes.vitality;
        int actualStrength = rogue.baseAttributes.strength;
        int actualDexterity = rogue.baseAttributes.dexterity;
        int actualIntelligence = rogue.baseAttributes.intelligence;

        int expectedVitality = 8;
        int expectedStrength = 2;
        int expectedDexterity = 6;
        int expectedIntelligence = 1;

        assertTrue(
                expectedVitality == actualVitality &&
                        expectedStrength == actualStrength &&
                        expectedDexterity == actualDexterity &&
                        expectedIntelligence == actualIntelligence
        );
    }

    @Test
    void Warrior_BaseAttributes_ShouldHaveCorrectValues() {

        Warrior warrior = new Warrior("Ranger1", 1);
        warrior.initializeBaseAttributes();

        int actualVitality = warrior.baseAttributes.vitality;
        int actualStrength = warrior.baseAttributes.strength;
        int actualDexterity = warrior.baseAttributes.dexterity;
        int actualIntelligence = warrior.baseAttributes.intelligence;

        int expectedVitality = 10;
        int expectedStrength = 5;
        int expectedDexterity = 2;
        int expectedIntelligence = 1;

        assertTrue(
                expectedVitality == actualVitality &&
                        expectedStrength == actualStrength &&
                        expectedDexterity == actualDexterity &&
                        expectedIntelligence == actualIntelligence
        );
    }

    @Test
    void Mage_LvlUp_ShouldIncreaseHeroAttributesCorrectly() {

        Mage mage = new Mage("Mage1", 1);
        mage.lvlUp(1);

        int actualVitality = mage.baseAttributes.vitality;
        int actualStrength = mage.baseAttributes.strength;
        int actualDexterity = mage.baseAttributes.dexterity;
        int actualIntelligence = mage.baseAttributes.intelligence;

        int expectedVitality = 8;
        int expectedStrength = 2;
        int expectedDexterity = 2;
        int expectedIntelligence = 13;

        assertEquals(true,
                expectedVitality == actualVitality &&
                        expectedStrength == actualStrength &&
                        expectedDexterity == actualDexterity &&
                        expectedIntelligence == actualIntelligence
        );

    }

    @Test
    void Ranger_LvlUp_ShouldIncreaseHeroAttributesCorrectly() {

        Ranger ranger = new Ranger("Mage1", 1);
        ranger.lvlUp(1);

        int actualVitality = ranger.baseAttributes.vitality;
        int actualStrength = ranger.baseAttributes.strength;
        int actualDexterity = ranger.baseAttributes.dexterity;
        int actualIntelligence = ranger.baseAttributes.intelligence;

        int expectedVitality = 10;
        int expectedStrength = 2;
        int expectedDexterity = 12;
        int expectedIntelligence = 2;

        assertEquals(true,
                expectedVitality == actualVitality &&
                        expectedStrength == actualStrength &&
                        expectedDexterity == actualDexterity &&
                        expectedIntelligence == actualIntelligence
        );
    }

    @Test
    void Rogue_LvlUp_ShouldIncreaseHeroAttributesCorrectly() {

        Rogue rogue = new Rogue("Mage1", 1);
        rogue.lvlUp(1);

        int actualVitality = rogue.baseAttributes.vitality;
        int actualStrength = rogue.baseAttributes.strength;
        int actualDexterity = rogue.baseAttributes.dexterity;
        int actualIntelligence = rogue.baseAttributes.intelligence;

        int expectedVitality = 11;
        int expectedStrength = 3;
        int expectedDexterity = 10;
        int expectedIntelligence = 2;

        assertEquals(true,
                expectedVitality == actualVitality &&
                        expectedStrength == actualStrength &&
                        expectedDexterity == actualDexterity &&
                        expectedIntelligence == actualIntelligence
        );
    }

    @Test
    void Warrior_LvlUp_ShouldIncreaseHeroAttributesCorrectly() {

        Warrior warrior = new Warrior("Mage1", 1);
        warrior.lvlUp(1);

        int actualVitality = warrior.baseAttributes.vitality;
        int actualStrength = warrior.baseAttributes.strength;
        int actualDexterity = warrior.baseAttributes.dexterity;
        int actualIntelligence = warrior.baseAttributes.intelligence;

        int expectedVitality = 15;
        int expectedStrength = 8;
        int expectedDexterity = 4;
        int expectedIntelligence = 2;

        assertEquals(true,
                expectedVitality == actualVitality &&
                        expectedStrength == actualStrength &&
                        expectedDexterity == actualDexterity &&
                        expectedIntelligence == actualIntelligence
        );
    }
}