package Item;

import Hero.Hero;
import Hero.Warrior;
import Hero.PrimaryAttribute;
import Exception.InvalidArmorException;
import Exception.InvalidWeaponException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ItemTests {

    @Test
    void Hero_EquipHighLvlWeapon_ShouldThrowInvalidWeaponException() {

        Warrior warrior = new Warrior("Warrior1", 1);
        Weapon axe = new Weapon(Weapon.Type.AXE, "iron axe", 2, Hero.Slot.WEAPON);

        assertThrows(Exception.class, () -> {
            warrior.equipWeapon(axe, Hero.Slot.WEAPON);
        });
    }

    @Test
    void Hero_EquipHighLvlArmor_ShouldThrowInvalidArmorException() {

        Warrior warrior = new Warrior("Warrior1", 1);
        Armor plateBody = new Armor(Armor.Type.PLATE, "iron plate body", 2, Hero.Slot.BODY);

        assertThrows(Exception.class, () -> {
            warrior.equipArmor(plateBody, Hero.Slot.BODY);
        });
    }

    @Test
    void Hero_EquipWrongWeaponType_ShouldThrowInvalidWeaponException() {

        Warrior warrior = new Warrior("Warrior1", 1);
        Weapon bow = new Weapon(Weapon.Type.BOW, "oak bow", 1, Hero.Slot.WEAPON);

        assertThrows(Exception.class, () -> {
            warrior.equipWeapon(bow, Hero.Slot.WEAPON);
        });
    }

    @Test
    void Hero_EquipWrongArmorType_ShouldThrowInvalidArmorException() {

        Warrior warrior = new Warrior("Warrior1", 1);
        Armor cloth = new Armor(Armor.Type.CLOTH, "shitty cloth", 1, Hero.Slot.HEAD);

        assertThrows(Exception.class, () -> {
            warrior.equipArmor(cloth, Hero.Slot.BODY);
        });
    }

    @Test
    void Hero_EquipValidWeapon_ShouldReturnTrue() {
        Warrior warrior = new Warrior("Warrior1", 1);
        Weapon axe = new Weapon(Weapon.Type.AXE, "bronze axe", 1, Hero.Slot.WEAPON);

        boolean actual = false;
        boolean expected = true;
        try {
            actual = warrior.equipWeapon(axe, Hero.Slot.WEAPON);
        } catch (InvalidWeaponException e) {
            System.out.println(e.getMessage());
        }

        assertEquals(expected, actual);
    }

    @Test
    void Hero_EquipValidArmorPiece_ShouldReturnTrue() {
        Warrior warrior = new Warrior("Warrior1", 1);
        Armor platebody = new Armor(Armor.Type.PLATE, "iron plate body", 1, Hero.Slot.BODY);

        boolean actual = false;
        boolean expected = true;
        try {
            actual = warrior.equipArmor(platebody, Hero.Slot.BODY);
        } catch (InvalidArmorException e) {
            System.out.println(e.getMessage());
        }

        assertEquals(expected, actual);
    }

    @Test
    void Hero_WithoutWeapon_ShouldHaveCorrectDPSValue() {
        Warrior warrior = new Warrior("Warrior1", 1);

        double actualDPS = warrior.getDPS();
        double expectedDPS = ( 1 * (1 + ((double)5/100)) );

        assertEquals(expectedDPS, actualDPS);
    }

    @Test
    void Hero_WithWeapon_shouldHaveCorrectDPSValue() {
        Warrior warrior = new Warrior("Warrior1", 1);
        Weapon axe = new Weapon(Weapon.Type.AXE, "iron axe", 1, Hero.Slot.WEAPON);
        axe.setDamage(7);
        axe.setAttackSpeed(1.1);
        try {
            warrior.equipWeapon(axe, Hero.Slot.WEAPON);
        } catch (InvalidWeaponException e) {}

        double actualDPS = warrior.getDPS();
        double expectedDPS = (7 * 1.1) * (1 + ((double)5/100));

        assertEquals(expectedDPS, actualDPS);
    }

    @Test
    void Hero_WithWeaponAndArmor_ShouldHaveCorrectDPSValue() {
        Warrior warrior = new Warrior("Warrior1", 1);
        Weapon axe = new Weapon(Weapon.Type.AXE, "iron axe", 1, Hero.Slot.WEAPON);
        axe.setDamage(7);
        axe.setAttackSpeed(1.1);
        try {
            warrior.equipWeapon(axe, Hero.Slot.WEAPON);
        } catch (InvalidWeaponException e) {}

        Armor plateBody = new Armor(Armor.Type.PLATE, "iron plate body", 1, Hero.Slot.BODY);
        plateBody.setStats(new PrimaryAttribute(2, 1, 0, 0));
        try {
            warrior.equipArmor(plateBody, Hero.Slot.BODY);
        } catch (InvalidArmorException e) {}

        double actualDPS = warrior.getDPS();
        double expectedDPS = (7 * 1.1) * (1 + ((double)(5+1)/100));

        assertEquals(expectedDPS, actualDPS);
    }

}