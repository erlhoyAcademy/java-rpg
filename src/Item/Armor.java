package Item;

import Hero.Hero;
import Hero.PrimaryAttribute;

public class Armor extends Item {

    PrimaryAttribute stats;

    public Armor(Type armorType, String name, int requiredLvl, Hero.Slot slot) {
        super(name, requiredLvl, slot);
        this.armorType = armorType;
    }

    public enum Type {
        CLOTH, LEATHER, MAIL, PLATE
    }


    private Type armorType;

    // GETTERS AND SETTERS
    public Type getArmorType() {
        return armorType;
    }

    // sets the stats (attributes) for the particular armor
    public void setStats(PrimaryAttribute stats) {
        this.stats = stats;
    }

    public PrimaryAttribute getStats() {
        return this.stats;
    }
}
