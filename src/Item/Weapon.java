package Item;

import Hero.Hero;

public class Weapon extends Item {

    public Weapon(Type weaponType, String name, int requiredLvl, Hero.Slot weaponSlot) {
        super(name, requiredLvl, weaponSlot);
        this.weaponType = weaponType;
    }

    private double damage;
    private double attackSpeed;
    private Type weaponType;

    public enum Type {
        AXE, BOW, DAGGER, HAMMER, STAFF, SWORD, WAND
    }

    // GETTERS AND SETTERS
    public double getDamage() {
        return damage;
    }

    public void setDamage(double damage) {
        this.damage = damage;
    }

    public double getAttackSpeed() {
        return attackSpeed;
    }

    public void setAttackSpeed(double attackSpeed) {
        this.attackSpeed = attackSpeed;
    }

    public Type getWeaponType() {
        return weaponType;
    }

    // returning the weapon's damage per second
    public double getDps() {
        return damage * attackSpeed;
    }
}
