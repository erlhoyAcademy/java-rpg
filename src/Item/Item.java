package Item;

import Hero.Hero;

public class Item {

    private String name;
    private int requiredLvl;
    private Hero.Slot slot; // could also be an integer, but using string for simplicity

    Item(String name, int requiredLvl, Hero.Slot slot) {
        this.name = name;
        this.requiredLvl = requiredLvl;
        this.slot = slot;
    }

    public int getRequiredLvl() {
        return requiredLvl;
    }
}
