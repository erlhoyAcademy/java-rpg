package Hero;

import Item.*;
import Exception.*;

public class Ranger extends Hero {

    Ranger(String name, int lvl) {
        super(name, lvl);
        initializeBaseAttributes();
    }

    @Override
    protected void initializeBaseAttributes() {
        this.baseAttributes = new PrimaryAttribute(8, 1, 7, 1);
    }

    @Override
    protected void IncreaseBaseAttributes() {
        this.baseAttributes.vitality += 2;
        this.baseAttributes.strength += 1;
        this.baseAttributes.dexterity += 5;
        this.baseAttributes.intelligence += 1;
    }

    @Override
    // returns the Ranger's total dexterity attribute value
    public int getHeroPrimaryTotalAttribute() {
        return getTotalPrimaryAttribute().dexterity;
    }

    @Override
    public boolean equipWeapon(Weapon weapon, Hero.Slot slot) throws InvalidWeaponException {
        if (weapon.getWeaponType() == Weapon.Type.BOW) {
            // check also if the hero has required lvl to equip the weapon
            if (this.level >= weapon.getRequiredLvl()) {
                // check if the slot is valid
                if (slot == Slot.WEAPON) {
                    equipment.put(slot, weapon);
                    return true;
                }
                else throw new InvalidWeaponException("The weapon can only be equipped in the weapon slot");
            }
            else throw new InvalidWeaponException("The hero has not the required level to equip this item");
        }
        else throw new InvalidWeaponException("The hero cannot equip this type of weapon");
    }

    @Override
    public boolean equipArmor(Armor armor, Hero.Slot slot) throws InvalidArmorException {
        if (armor.getArmorType() == Armor.Type.LEATHER || armor.getArmorType() == Armor.Type.MAIL) {
            // check also if the hero has required level to equip the armor
            if (this.level >= armor.getRequiredLvl()) {
                // check if the slot is valid
                if (slot != Slot.WEAPON) {
                    equipment.put(slot, armor);
                    return true;
                }
                else throw new InvalidArmorException("The armor cannot be equipped in the weapon slot");
            }
            else throw new InvalidArmorException("The hero has not the required level to equip this item");
        }
        else throw new InvalidArmorException("The hero cannot equip this type of armor");
    }
}
