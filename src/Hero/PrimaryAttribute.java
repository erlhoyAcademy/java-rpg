package Hero;

public class PrimaryAttribute {

    public PrimaryAttribute(int vitality, int strength, int dexterity, int intelligence) {
        this.vitality = vitality;
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }
    protected int vitality; //determiners hero's health
    protected int strength; //hero's strength
    protected int dexterity; //attack with speed and nimbleneses
    protected int intelligence; //hero's affinity with mage

}
