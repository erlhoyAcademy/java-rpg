package Hero;

import Item.*;
import Exception.*;

public class Mage extends Hero {

    public Mage(String name, int lvl) {
        super(name, lvl);
        initializeBaseAttributes();
    }

    @Override
    protected void initializeBaseAttributes() {
        this.baseAttributes = new PrimaryAttribute(5, 1, 1, 8);
    }

    @Override
    protected void IncreaseBaseAttributes() {
        this.baseAttributes.vitality += 3;
        this.baseAttributes.strength += 1;
        this.baseAttributes.dexterity += 1;
        this.baseAttributes.intelligence += 5;
    }

    @Override
    // returns the Mage's total Intelligence attribute value
    public int getHeroPrimaryTotalAttribute() {
        return getTotalPrimaryAttribute().intelligence;
    }

    @Override
    public boolean equipWeapon(Weapon weapon, Hero.Slot slot) throws InvalidWeaponException {
        if (weapon.getWeaponType() == Weapon.Type.STAFF || weapon.getWeaponType() == Weapon.Type.WAND) {
            // check also if the hero has required lvl to equip the weapon
            if (this.level >= weapon.getRequiredLvl()) {
                // check if the slot is valid
                if (slot == Slot.WEAPON) {
                    equipment.put(slot, weapon);
                    return true;
                }
                else throw new InvalidWeaponException("The weapon can only be equipped in the weapon slot");
            }
            else throw new InvalidWeaponException("The hero has not the required level to equip this item");
        }
        else throw new InvalidWeaponException("The hero cannot equip this type of weapon");
    }

    @Override
    public boolean equipArmor(Armor armor, Hero.Slot slot) throws InvalidArmorException {
        if (armor.getArmorType() == Armor.Type.CLOTH) {
            // check also if the hero has required level to equip the armor
            if (this.level >= armor.getRequiredLvl()) {
                // check if the slot is valid
                if (slot != Slot.WEAPON) {
                    equipment.put(slot, armor);
                    return true;
                }
                else throw new InvalidArmorException("The armor cannot be equipped in the weapon slot");
            }
            else throw new InvalidArmorException("The hero has not the required level to equip this item");
        }
        else throw new InvalidArmorException("The hero cannot equip this type of armor");
    }
}
