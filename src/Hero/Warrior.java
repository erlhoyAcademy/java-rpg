package Hero;
import Item.*;
import Exception.*;

public class Warrior extends Hero {

    public Warrior(String name, int lvl) {
        super(name, lvl);
        initializeBaseAttributes();
    }

    @Override
    protected void initializeBaseAttributes() {
        this.baseAttributes = new PrimaryAttribute(10, 5, 2, 1);
    }

    @Override
    protected void IncreaseBaseAttributes() {
        this.baseAttributes.vitality += 5;
        this.baseAttributes.strength += 3;
        this.baseAttributes.dexterity += 2;
        this.baseAttributes.intelligence += 1;
    }

    @Override
    // returns the Warrior's total strength attribute value
    public int getHeroPrimaryTotalAttribute() {
        return getTotalPrimaryAttribute().strength;
    }

    @Override
    public boolean equipWeapon(Weapon weapon, Hero.Slot slot) throws InvalidWeaponException {
        if (weapon.getWeaponType() == Weapon.Type.AXE || weapon.getWeaponType() == Weapon.Type.HAMMER || weapon.getWeaponType() == Weapon.Type.SWORD) {
            // check also if the hero has required lvl to equip the weapon
            if (this.level >= weapon.getRequiredLvl()) {
                // check if the slot is valid
                if (slot == Slot.WEAPON) {
                    equipment.put(slot, weapon);
                    return true;
                }
                else throw new InvalidWeaponException("The weapon can only be equipped in the weapon slot");
            }
            else throw new InvalidWeaponException("The hero has not the required level to equip this item");
        }
        else throw new InvalidWeaponException("The hero cannot equip this type of weapon");
    }

    @Override
    public boolean equipArmor(Armor armor, Hero.Slot slot) throws InvalidArmorException {
        if (armor.getArmorType() == Armor.Type.MAIL || armor.getArmorType() == Armor.Type.PLATE) {
            // check also if the hero has required level to equip the armor
            if (this.level >= armor.getRequiredLvl()) {
                // check if the slot is valid
                if (slot != Slot.WEAPON) {
                    equipment.put(slot, armor);
                    return true;
                }
                else throw new InvalidArmorException("The armor cannot be equipped in the weapon slot");
            }
            else throw new InvalidArmorException("The hero has not the required level to equip this item");
        }
        else throw new InvalidArmorException("The hero cannot equip this type of armor");
    }
}
