package Hero;
import Item.*;
import Exception.*;

import java.util.HashMap;

public abstract class Hero {

    // Hero's base attributes: VITALITY, STRENGTH, DEXTERITY, INTELLIGENCE
    public PrimaryAttribute baseAttributes;

    // Hero's slot types where armor or weapon can be equipped
    public enum Slot {
        HEAD, BODY, LEGS, WEAPON
    }

    // Hero's currently equipped items are stored in this HashMap
    protected HashMap<Slot, Item> equipment;

    protected String name;
    protected int level;

    // Constructor called when creating a new Hero
    Hero(String name, int lvl) {
        this.name = name;
        this.level = lvl;
        equipment = new HashMap<Slot, Item>();
    }

    protected void lvlUp(int n) { //taking in an integer, with flexibility to level up more than once
        this.level += n;
        for (int i = 0; i < n; i++) {
            IncreaseBaseAttributes();
        }
    }

    //
    public double getDPS() {
        //if the Hero has a weapon equipped
        if (equipment.containsKey(Slot.WEAPON)) {
            //get the equipped weapon
            Weapon weapon = (Weapon)equipment.get(Slot.WEAPON);

            return weapon.getDps() * (1 + (double)getHeroPrimaryTotalAttribute() / 100);
        }
        return 1 + (double)getHeroPrimaryTotalAttribute() / 100; // if hero does not have weapon equipped, the weapon factor is taken away
    };

    protected abstract int getHeroPrimaryTotalAttribute(); // should return TotalPrimaryAttribute.strength for warrior, .intelligence for mage, etc.

    //adds the values of two PrimaryAttributes to a new single PrimaryAttribute
    public PrimaryAttribute addAttributes(PrimaryAttribute a1, PrimaryAttribute a2) {
        PrimaryAttribute result = new PrimaryAttribute(
              a1.vitality + a2.vitality,
                a1.strength + a2.strength,
                a1.dexterity + a2.dexterity,
                a1.intelligence + a2.intelligence
        );
        return result;
    }

    // returns the total value of base attributes + attributes provided by armor
    public PrimaryAttribute getTotalPrimaryAttribute() {
        // make a copy of the base attributes, for initialization
        PrimaryAttribute totalAttributes = new PrimaryAttribute(
                this.baseAttributes.vitality,
                this.baseAttributes.strength,
                this.baseAttributes.dexterity,
                this.baseAttributes.intelligence
        );
        // add the stats provided by the armor to the base attributes - resulting in total attributes
        if (equipment.containsKey(Slot.HEAD)) {
            Armor headArmor = (Armor)equipment.get(Slot.HEAD);
            totalAttributes = addAttributes(totalAttributes, headArmor.getStats());
        }
        if (equipment.containsKey(Slot.BODY)) {
            Armor bodyArmor = (Armor)equipment.get(Slot.BODY);
            totalAttributes = addAttributes(totalAttributes, bodyArmor.getStats());
        }
        if (equipment.containsKey(Slot.LEGS)) {
            Armor legsArmor = (Armor)equipment.get(Slot.LEGS);
            totalAttributes = addAttributes(totalAttributes, legsArmor.getStats());
        }

        return totalAttributes;
    }

    // abstract methods:
    protected abstract void IncreaseBaseAttributes();

    protected abstract void initializeBaseAttributes();

    protected abstract boolean equipWeapon(Weapon weapon, Slot slot) throws InvalidWeaponException;

    protected abstract boolean equipArmor(Armor armor, Slot slot) throws InvalidArmorException;



}
